#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>
#include <omp.h>
#include <sys/time.h>

using namespace std;

static const int GRID_SIZE = 16384;
float heat1[GRID_SIZE][GRID_SIZE];
float heat2[GRID_SIZE][GRID_SIZE];
static const bool DEBUG = false;
static const bool VERBOSE = false;

bool IsLockedCell(int i, int j)
{
	if (i == 0 || j == 0 || i == GRID_SIZE - 1 || j == GRID_SIZE - 1)
		return true;

	return false;
}

double When()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

int main()
{
	double startTime = When();

	// Set all cells at 50 to start
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			heat1[i][j] = 50;
		}
	}

	// Set the fixed cells
	for (int i = 0; i < GRID_SIZE; i++)
	{
		heat1[i][0] = 0;
		heat1[i][GRID_SIZE - 1] = 0;
	}

	for (int j = 0; j < GRID_SIZE; j++)
	{
		heat1[0][j] = 0;
		heat1[GRID_SIZE - 1][j] = 100;
	}

	if (DEBUG) cout << "Heat 1 initialized" << endl;

	int iterCount = 0;
	bool readHeat1 = true;
	bool steady = false;

	float(*readPointer)[GRID_SIZE][GRID_SIZE];
	float(*writePointer)[GRID_SIZE][GRID_SIZE];

	while (!steady)
	{

		// Switch tables to read/write
		if (readHeat1)
		{
			readPointer = &heat1;
			writePointer = &heat2;
		}
		else
		{
			readPointer = &heat2;
			writePointer = &heat1;
		}

		// if (DEBUG) cout << "(200,500): " << (*readPointer)[200][500] << endl;
		#pragma omp parallel for
		for (int i = 0; i < GRID_SIZE; i++)
		{
			for (int j = 0; j < GRID_SIZE; j++)
			{
				if (VERBOSE) cout << "Setting writePointer[" << i << "][" << j << "]" << endl;
				if (IsLockedCell(i, j))
					(*writePointer)[i][j] = (*readPointer)[i][j];
				else
					(*writePointer)[i][j] = ((*readPointer)[i + 1][j] + (*readPointer)[i - 1][j] + (*readPointer)[i][j + 1] + (*readPointer)[i][j - 1] + (4 * (*readPointer)[i][j])) / 8;
			}
		}

		iterCount++;
		bool notSteady = false;
		float state;

		for (int i = 0; i < GRID_SIZE - 1; i++)
		{
			for (int j = 0; j < GRID_SIZE - 1; j++)
			{
				if(IsLockedCell(i,j))
					continue;
				state = fabs((*writePointer)[i][j] - ((*writePointer)[i + 1][j] + (*writePointer)[i - 1][j] + (*writePointer)[i][j + 1] + (*writePointer)[i][j - 1]) / 4);
				if (state >= 0.1)
				{
					if (DEBUG) cout << "Not Steady found, state: " << state << " writeP[" << i << "][" << j << "]" << "; readP: " << (*readPointer)[i][j] << "; writeP: " << (*writePointer)[i][j] << endl;
					notSteady = true;
					break;
				}
			}
			if (notSteady) break;
		}
		steady = !notSteady;
		readHeat1 = !readHeat1;
		if (DEBUG) cout << "Current Iterations: " << iterCount << endl;
	}

	double endTime = When();
	cout << "Total Iterations: " << iterCount << "; Total Time: " << endTime - startTime << endl;
	return 0;
}
