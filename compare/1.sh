#!/bin/bash

#SBATCH --time=00:05:00   # walltime
#SBATCH --nodes=1   # number of nodes
#SBATCH --mem-per-cpu=4096   # memory per CPU core
#SBATCH -J "Project 1"   # job name
#SBATCH --mail-user=sean@seanlane.net   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --qos=test

# Compatibility variables for PBS. Delete if not needed.
export PBS_NODEFILE=`/fslapps/fslutils/generate_pbs_nodefile`
export PBS_JOBID=$SLURM_JOB_ID
export PBS_O_WORKDIR="$SLURM_SUBMIT_DIR"
export PBS_QUEUE=batch

# Set the max number of threads to use for programs using OpenMP. Should be <= ppn. Does nothing if the program doesn't use OpenMP.
export OMP_NUM_THREADS=$1

# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
./One

