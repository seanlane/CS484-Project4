#!/bin/bash

#SBATCH --time=00:05:00   # walltime
#SBATCH --mem-per-cpu=4069M   # memory per CPU core
#SBATCH -J "Project 2"   # job name
#SBATCH --mail-user=sean@seanlane.net   # email address
#SBATCH --mail-type=BEGIN
#SBATCH --mail-type=END
#SBATCH --mail-type=FAIL
#SBATCH --qos=test

# Compatibility variables for PBS. Delete if not needed.
export PBS_NODEFILE=`/fslapps/fslutils/generate_pbs_nodefile`
export PBS_JOBID=$SLURM_JOB_ID
export PBS_O_WORKDIR="$SLURM_SUBMIT_DIR"
export PBS_QUEUE=batch


# LOAD MODULES, INSERT CODE, AND RUN YOUR PROGRAMS HERE
./Two $1
