#include <iostream>
#include <sstream>
#include <fstream>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include <pthread.h>
#include <stdlib.h>

using namespace std;

const int MAX_THREADS = 32;
int NUM_THREADS;
const int GRID_SIZE = 16384;
static const bool DEBUG = false;
static const bool VERBOSE = false;
float ** heat1 = new float*[GRID_SIZE];
float ** heat2 = new float*[GRID_SIZE];
float ** readPointer;
float ** writePointer;

// Barriers
pthread_barrier_t top_barrier;
pthread_barrier_t middle_barrier;
pthread_barrier_t bottom_barrier;

// Thread Struct and Array
struct thread_data {
	int thread_id;
	int * iterCount;
	int fifty_cell_count;
	int row_start_index;
	int row_end_index;
	bool * steady;
	float max_diff;
};

struct thread_data thread_data_array [MAX_THREADS];

bool IsLockedCell(int i, int j)
{
	if( i == 0 || j == 0)
		return true;

	if( i == GRID_SIZE - 1 || j == GRID_SIZE - 1)
		return true;
	
	return false;
}

double When()
{
	struct timeval tp;
	gettimeofday(&tp, NULL);
	return ((double)tp.tv_sec + (double)tp.tv_usec * 1e-6);
}

void InitializeGrids()
{
	// Initialize the grids
	for (int i = 0; i < GRID_SIZE; i++)
	{
		heat1[i] = new float[GRID_SIZE];
		heat2[i] = new float[GRID_SIZE];
	}

	// Set all cells at 50 to start
	for (int i = 0; i < GRID_SIZE; i++)
	{
		for (int j = 0; j < GRID_SIZE; j++)
		{
			heat1[i][j] = 50;
		}
	}

	// Set the fixed cells
	for (int j = 0; j < GRID_SIZE; j++)
	{
		// Top row to 0
		heat1[0][j] = 0;
		// Bottom row to 100
		heat1[GRID_SIZE - 1][j] = 100;

		// Left and right column to 0
		heat1[j][0] = heat1[j][GRID_SIZE - 1] = 0;
	}

	int temp_count = 0;
	if (VERBOSE) 
	{
		cout << "Heat 1 initialized" << endl;
		for(int i = 0; i < GRID_SIZE; i++)
		{
			for(int j = 0; j < GRID_SIZE; j++)
			{
				if( heat1[i][j] > 50) temp_count++;		
			}
		}
		cout << "Cells over 50 degrees: " << temp_count << endl;
	}	
}

void WriteCellCountToFile(int count)
{
	ofstream file;
	file.open("cell.csv", ios_base::app);
	file << count << endl;
	file.close();
}

void * HotPlate(void * thread_arg)
{
	struct thread_data * data = (struct thread_data *) thread_arg;
	int rc;

	if(DEBUG)
	{
		cout << "My Thread ID is " << data->thread_id << "; Start Row: " << data->row_start_index;
		cout << "; End Row (Exclusive): " << data->row_end_index << endl;
	}

	while (!(*data->steady))
	{
		data->fifty_cell_count = 0;
		for (int i = data->row_start_index; i < data->row_end_index; i++)
		{
			// if (DEBUG && (i % 50 == 0)) cout << "On Row #: " << i << endl;			
			for (int j = 0; j < GRID_SIZE; j++)
			{
				if (IsLockedCell(i, j))
					writePointer[i][j] = readPointer[i][j];
				else
					writePointer[i][j] = (readPointer[i + 1][j] + readPointer[i - 1][j] + readPointer[i][j + 1] + readPointer[i][j - 1] + (4 * readPointer[i][j])) / 8;
				if(writePointer[i][j] > 50)
					data->fifty_cell_count++;
			}
		}

		// Top Barrier - Wait for all threads to create the grid for this iteration
		rc = pthread_barrier_wait(&top_barrier);
		if( rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
		{
			cout << "ERROR: Could not wait on top_barrier!\n";
			return (void *) -1;
		}

		data->max_diff = 0;
		for (int i = data->row_start_index; i < data->row_end_index; i++)
		{
			for (int j = 1; j < GRID_SIZE - 1; j++)
			{
				if(IsLockedCell(i,j))
					continue;
				data->max_diff = fmax(data->max_diff, fabs(writePointer[i][j] - (writePointer[i + 1][j] + writePointer[i - 1][j] + writePointer[i][j + 1] + writePointer[i][j - 1]) / 4));
				if (data->max_diff >= 0.1)
				{
					if (VERBOSE)
					{ 
						cout << "Not Steady found, max_diff: " << data->max_diff << " writeP[" << i;
						cout << "][" << j << "]" << "; readP: " << readPointer[i][j] << "; writeP: " << writePointer[i][j] << endl;
					}
					break;
				}
			}
			if (data->max_diff >= 0.1) 
				break;
		}
		
		// Middle Barrier - Wait on threads to finish calculating the max_diff of the states
		rc = pthread_barrier_wait(&middle_barrier);
		if( rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
		{
			cout << "ERROR: Could not wait on middle_barrier!\n";
			return (void *) -1;
		}
		
		// Switch tables to read/write
		if (data->thread_id == 0)
		{
			(*data->iterCount)++;
		 	
			// Here, we should see if all the threads have a steady state, also add up the cells over 50 degrees (?)	
			int cell_count = 0;
			for(int i = 0; i < NUM_THREADS; i++)	
				cell_count += thread_data_array[i].fifty_cell_count;
			if (VERBOSE) WriteCellCountToFile(cell_count);
	
			for(int i = 0; i < NUM_THREADS; i++)
			{
				data->max_diff = fmax(data->max_diff, thread_data_array[i].max_diff);
				if(thread_data_array[i].max_diff >= 0.1)
					break;
			}
			if(data->max_diff < 0.1)
				(*data->steady) = true;
			

			float ** tempPointer = readPointer;
			readPointer = writePointer;
			writePointer = tempPointer;
			
			if (DEBUG) cout << "Current Iterations: " << (*data->iterCount) << endl;
		}	
		
		// Bottom Barrier - Wait for thread 0 to calulate the total cells over 50 degrees, see if we need to keep going
		rc = pthread_barrier_wait(&bottom_barrier);
		if( rc != 0 && rc != PTHREAD_BARRIER_SERIAL_THREAD)
		{
			cout << "ERROR: Could not wait on bottom_barrier!\n";
			return (void *) -1;
		}
	}
	pthread_exit(NULL);
}

int main(int argc, char* argv[])
{
	double startTime = When();

	NUM_THREADS = atoi(argv[1]);

	InitializeGrids();
		
	readPointer = heat1;
	writePointer = heat2;
	bool steady = false;
	int iterCount = 0;
	int rc;

	// Initialize the barriers
	if(pthread_barrier_init(&top_barrier, NULL, NUM_THREADS))
	{
		cout << "ERROR: Could not initialize top_barrier\n";
		return -1;
	}	
	if(pthread_barrier_init(&middle_barrier, NULL, NUM_THREADS))
	{
		cout << "ERROR: Could not initialize middle_barrier\n";
		return -1;
	}
	if(pthread_barrier_init(&bottom_barrier, NULL, NUM_THREADS))
	{
		cout << "ERROR: Could not initialize bottom_barrier\n";
		return -1;
	}

	// Initialize the threads
	pthread_t thread[MAX_THREADS];
	pthread_attr_t attr;
	int total_fifty_cell_count;

	// Initialize the thread attributes
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	for(int t = 0; t < NUM_THREADS; t++)
	{
		thread_data_array[t].thread_id = t;
		thread_data_array[t].iterCount = &iterCount;
		thread_data_array[t].steady = &steady;
		thread_data_array[t].row_start_index = (GRID_SIZE/NUM_THREADS) * t;
		if(t < NUM_THREADS - 1)
			thread_data_array[t].row_end_index = (GRID_SIZE/NUM_THREADS) * (t + 1);
		else
			thread_data_array[t].row_end_index = GRID_SIZE;
		rc = pthread_create(&thread[t], &attr, HotPlate, (void *) &thread_data_array[t]);
		if(rc) 
		{
			cout << "ERROR: Return code from pthread_create(): " << rc << endl;
			return(-1);
		}  
	}

	pthread_attr_destroy(&attr);
	for(int t = 0; t < NUM_THREADS; t++)
	{
		rc = pthread_join(thread[t], NULL);
		if(rc)
		{
			cout << "ERROR: Return code from pthread_join(): " << rc << endl;
			return(-1);
		}

	}

	double endTime = When();
	cout << "NProc: " << NUM_THREADS << " Total Iterations: " << iterCount << "; Total Time: " << endTime - startTime << endl;
	// cout << "Total Iterations: " << iterCount << endl;

	// Delete the grids
	// Initialize the grids
	for (int i = 0; i < GRID_SIZE; i++)
	{
		delete heat1[i];
		delete heat2[i];
	}

	delete heat1;
	delete heat2;

	return 0;
}
