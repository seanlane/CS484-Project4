linear:
	mpicc -O -lm LinearRelax.c -o LinearRelax

hotplate:	MPIHotplate.cpp
	mpic++ -O MPIHotplate.cpp -o MPIHotplate -lm

run:
	sbatch run.sh
