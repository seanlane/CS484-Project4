#include <mpi.h>
#include <stdio.h>
#include <sys/time.h>

#define SIZE     16384
#define EPSILON  0.1
#define DEBUG	false

float fabs(float f);
double When();
void CellInfo(float ** grid, int row, int col);

int main(int argc, char *argv[])
{
	float **write, **read, **temp;
	int cnt, start, end, theSize;
	int steady, local_steady;

    double starttime;

    int nproc, iproc;
    MPI_Status status;

    MPI_Init(&argc, &argv);
    starttime = When();

    MPI_Comm_size(MPI_COMM_WORLD, &nproc);
    MPI_Comm_rank(MPI_COMM_WORLD, &iproc);
    if(DEBUG) fprintf(stdout,"%d: Hello from %d of %d\n", iproc, iproc, nproc);
    
    /* Determine how much I should be doing and allocate the arrays*/
    theSize = SIZE / nproc;
    write = new float* [(theSize + 2) * sizeof(float*)];
	read = new float* [(theSize + 2) * sizeof(float*)];

    start = 1;
    end = theSize + 1;

    /* Initialize the cells */
    for (int i = 0; i < (theSize + 2); i++)
    {
		write[i] = new float[SIZE * sizeof(float)];
		read[i] = new float[SIZE * sizeof(float)];
		for (int j = 1; j < (SIZE - 1); j++)
			read[i][j] = 50;
		read[i][0] = read[i][SIZE - 1] = write[i][0] = write[i][SIZE - 1] = 0;
    }

    /* Initialize the Boundaries */
    if (iproc == 0)
    {
        start = 2;
		for (int j = 0; j < SIZE; j++)
			read[1][j] = write[1][j] = 0;
    }
    if (iproc == nproc - 1)
    {
        end = theSize;
		for (int j = 0; j < SIZE; j++)
			read[theSize][j] = write[theSize][j] = 100;
    }

	steady = 0;
    /* Now run the Hot Plate */
    for(cnt = 0; !steady; cnt++)
    {
		if (DEBUG && iproc == 0) fprintf(stdout, "Iteration #: %d; Steady is: %d\n", cnt, steady);
		
		/* First, I must get my neighbors boundary values */
        
		if ((iproc % 2) == 0)
		{
			if (iproc != 0)
			{
				MPI_Send(read[1], SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD);
				MPI_Recv(read[0], SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
			}
			if (iproc != nproc - 1)
			{
				MPI_Send(read[theSize], SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD);
				MPI_Recv(read[theSize + 1], SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
			}
		}
		else
		{
			if (iproc != nproc - 1)
			{
				MPI_Recv(read[theSize + 1], SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD, &status);
				MPI_Send(read[theSize], SIZE, MPI_FLOAT, iproc + 1, 0, MPI_COMM_WORLD);
			}
			if (iproc != 0)
			{
				MPI_Recv(read[0], SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD, &status);
				MPI_Send(read[1], SIZE, MPI_FLOAT, iproc - 1, 0, MPI_COMM_WORLD);
			}
		}
		
			/*if (DEBUG) fprintf(stdout, "Proc: %d; Start head vector send to Proc: %d\n", iproc, iproc - 1);
			if (DEBUG) fprintf(stdout, "Proc: %d; End send head, start receive head vector from Proc: %d\n", iproc, iproc - 1);
			if (DEBUG) fprintf(stdout, "Proc: %d; End receive head vector\n", iproc);
			
			if (DEBUG) fprintf(stdout, "Proc: %d; Start send tail vector to Proc: %d\n", iproc, iproc + 1);
			if (DEBUG) fprintf(stdout, "Proc: %d; End send tail, start receive tail vector from Proc: %d\n", iproc, iproc + 1);
            if (DEBUG) fprintf(stdout, "Proc: %d; End receive tail vector\n", iproc);*/
		
        /* Do the calculations */
        for (int i = start; i < end; i++)
        {
			for (int j = 1; j < (SIZE - 1); j++)
				write[i][j] = (read[i + 1][j] + read[i - 1][j] + read[i][j + 1] + read[i][j - 1] + (4 * read[i][j])) / 8;
        }

        /* Check to see if we are done */
        local_steady = 1;
        for (int i = start; i < end; i++)
        {
			for (int j = 1; j < (SIZE - 1); j++)
			{
				if (fabs(write[i][j] - (write[i + 1][j] + write[i - 1][j] + write[i][j + 1] + write[i][j - 1]) / 4) > EPSILON)
				{
					local_steady = 0;
					if (DEBUG && iproc == 0)
					{
						float delta = fabs(write[i][j] - (write[i + 1][j] + write[i - 1][j] + write[i][j + 1] + write[i][j - 1]) / 4);
						fprintf(stdout, "Breaking on [i][j]: [%d][%d]; Delta: %f\n", i, j, delta);
					}
					break;
				}
			}
			if (!local_steady)
				break;
        }
        /* Do a reduce to see if everybody is done */
        MPI_Allreduce(&local_steady, &steady, 1, MPI_INT, MPI_MIN, MPI_COMM_WORLD);

        /* Swap the pointers */
		temp = read;
        read = write;
        write = temp;
    }

    /* print out the number of iterations to relax */
	if (iproc == 0) fprintf(stderr, "Proc: %d; Iterations: %d; Time: %lf seconds\n",
                                   nproc,cnt,When() - starttime);

	/* Delete the allocated arrays */
	for (int i = 0; i < (theSize + 2); i++)
	{
		delete[] write[i];
		delete[] read[i];
	}

	delete[] write;
	delete[] read;

    MPI_Finalize();

	return 0;
}
    
float fabs(float f)
{
    return (f > 0.0)? f : -f ;
}

/* Return the correct time in seconds, using a double precision number.       */
double When()
{
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

/* Return if cell is a constant cell*/
bool IsLockedCell(int row, int col, int start, int end)
{
	return (row == start || row == (end - 1) || col == 0 || col == (SIZE - 1));
}

void CellInfo(float ** grid, int row, int col)
{
	float delta = fabs(grid[row][col] - (grid[row + 1][col] + grid[row - 1][col] + grid[row][col + 1] + grid[row][col - 1]) / 4);
	fprintf(stdout, "[%d][%d]: %f; delta: %f\n", row, col, grid[row][col], delta);

	fprintf(stdout, "[%d][%d]: %f\n", row - 1, col, grid[row - 1][col]);
	fprintf(stdout, "[%d][%d]: %f\n", row + 1, col, grid[row + 1][col]);
	fprintf(stdout, "[%d][%d]: %f\n", row, col - 1, grid[row][col - 1]);
	fprintf(stdout, "[%d][%d]: %f\n", row, col + 1, grid[row][col + 1]);
}
